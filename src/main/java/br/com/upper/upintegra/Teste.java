package br.com.upper.upintegra;

import br.com.upper.upintegra.persistencia.ConexaoTerceiro;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author FILIPE
 */
public class Teste {

    private ConexaoTerceiro conexao;
    private Connection connection;

    public Teste() throws IOException {
        conexao = ConexaoTerceiro.getInstance();
    }

    public void integridade() throws SQLException {
        connection = conexao.createConnection();
        DatabaseMetaData mt = connection.getMetaData();

        System.out.println("Versao do Driver JDBC = " + mt.getDriverVersion());
        System.out.println("Versao do Banco de Dados = " + mt.getDatabaseProductVersion());
        System.out.println("Suporta Select for Update? = " + mt.supportsSelectForUpdate());
        System.out.println("Suporta Transacoes? = " + mt.supportsTransactions());

        System.out.println("\n");

        ResultSet rs = mt.getSchemas();
        int i = 0;
        while (rs.next()) {
            System.out.println("SCHEMA DO BD = " + rs.getString(i + 1));
        }
    }

    public void colunas() throws SQLException {
        connection = conexao.createConnection();
        DatabaseMetaData mt = connection.getMetaData();
        Statement stmt = connection.createStatement();
        // Tabela a ser analisada  
        ResultSet rset = stmt.executeQuery("SELECT * from TA101 ");

        ResultSetMetaData rsmd = rset.getMetaData();

        // retorna o numero total de colunas  
        int numColumns = rsmd.getColumnCount();
        System.out.println("Total de Colunas = " + numColumns);

        // loop para recuperar os metadados de cada coluna  
        for (int i = 0; i < numColumns; i++) {
            System.out.print(rsmd.getTableName(i + 1));
            System.out.print(" | " + rsmd.getColumnName(i + 1));
            System.out.print(" | " + rsmd.getColumnType(i + 1));
            System.out.print(" | " + rsmd.getColumnTypeName(i + 1));
            System.out.print(" | " + rsmd.getColumnDisplaySize(i + 1));
            System.out.print(" | " + String.valueOf(rsmd.getPrecision(i + 1)));
            System.out.println(" | " + rsmd.getScale(i + 1));
        }
    }

    public static void main(String[] args) throws IOException {
        Teste teste = new Teste();

        try {
            teste.colunas();
            System.out.println(ConexaoTerceiro.getProp().getProperty("driver"));
        } catch (SQLException ex) {
            Logger.getLogger(Teste.class.getName()).log(Level.SEVERE, null, ex);
        }

//        MetaData mt = new MetaData();
//        List<String> tabs = mt.recuperaTabela();
//       
//        for (String string : tabs) {
//            System.out.println(string);
//        }
    }

}
