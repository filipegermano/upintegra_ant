package br.com.upper.upintegra.persistencia;

import br.com.upper.upintegra.entidades.Coluna;
import br.com.upper.upintegra.entidades.Integridade;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author FILIPE
 */
public class IntegridadeDAO {

    private ConexaoTerceiro conexaoTerceiro;
    private Connection connection;

    public IntegridadeDAO() {
        conexaoTerceiro = ConexaoTerceiro.getInstance();
    }

    public List<Integridade> listaIntegridades() throws SQLException {
        List<Integridade> integridades = new ArrayList<>();
        String sql = "select * from axi where xi_status <> 0";

        connection = conexaoTerceiro.createConnection();
        PreparedStatement pst = connection.prepareStatement(sql);

        ResultSet rs = pst.executeQuery();

        while (rs.next()) {
            Integridade integridade = new Integridade();
            integridade.setTabela(rs.getString("xi_tabela"));
            integridade.setChave(rs.getString("xi_chave"));
            integridade.setStatus(rs.getInt("xi_status"));
            integridade.setData(rs.getTimestamp("xi_data"));

            integridades.add(integridade);
        }
        rs.close();
        connection.close();
        pst.close();

        return integridades;
    }

    public void updateStatus(Integridade integridade) {
        PreparedStatement pst = null;
        try {
            String sql = "update axi set xi_status = 0 where xi_tabela = ? and xi_chave = ?";
                        
            connection = conexaoTerceiro.createConnection();
            pst = connection.prepareStatement(sql);
            
            pst.setString(1, integridade.getTabela());
            pst.setString(2, integridade.getChave());

            pst.executeUpdate();
            
            connection.close();
            pst.close();            
        } catch (SQLException ex) {
            Logger.getLogger(IntegridadeDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            try {
                connection.close();            
                pst.close();
            } catch (SQLException ex) {
                Logger.getLogger(IntegridadeDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
        
    public String recuperaIntegridade(Integridade integridade) throws SQLException{
        
        String sql = "select * from "+integridade.getTabela() + " where "
                +integridade.getChave();

        connection = conexaoTerceiro.createConnection();
        PreparedStatement pst = connection.prepareStatement(sql);
        ResultSet rs =  pst.executeQuery();
        
        
        String result = null;
        List<String> lista = new ArrayList<>();
        while(rs.next()){                        
        }
        
        pst.close();
        connection.close();
        rs.close();
        return result;
    }

}
