package br.com.upper.upintegra.persistencia.tests;

import br.com.upper.upintegra.entidades.Tabela;
import br.com.upper.upintegra.persistencia.AtualizaMetaData;
import br.com.upper.upintegra.persistencia.MetaData;
import java.sql.SQLException;

/**
 *
 * @author FILIPE
 */
public class AtualizaMetaDataLocal {
    
    public static void main(String[] args) throws SQLException {
        MetaData md = new MetaData();
        AtualizaMetaData amd = new AtualizaMetaData();
        Tabela tabela = new Tabela();
        tabela.setTabela("TA101");

       // System.out.println(amd.criaTrigger(tabela));        
//        System.out.println(amd.criaTrigger(tabela).substring(0, 500));
//        System.out.println(amd.criaTrigger(tabela).substring(500, 1000));
//        System.out.println(amd.criaTrigger(tabela).substring(1000, 1500));
//        System.out.println(amd.criaTrigger(tabela).substring(1500, 2000));
//        System.out.println(amd.criaTrigger(tabela).substring(2000, 2500));
//        System.out.println(amd.criaTrigger(tabela).substring(2500, 3000));
//        System.out.println(amd.criaTrigger(tabela).substring(3000, 3500));
//        System.out.println(amd.criaTrigger(tabela).substring(3500, 4000));
//        System.out.println(amd.criaTrigger(tabela).substring(4000, 4500));
//        System.out.println(amd.criaTrigger(tabela).substring(4500));
        
        System.out.println(md.executeQueryTrigger(amd.criaTrigger(tabela)));
                
    }
    
}
