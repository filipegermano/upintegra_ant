package br.com.upper.upintegra.persistencia.tests;

import br.com.upper.upintegra.entidades.Coluna;
import br.com.upper.upintegra.entidades.Tabela;
import br.com.upper.upintegra.persistencia.MetaData;
import br.com.upper.upintegra.persistencia.TabelaDAO;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author FILIPE
 */
public class Teste {
    
    public static void main(String[] args) throws SQLException {
        TabelaDAO tabelaDAO = new TabelaDAO();
        
        Coluna coluna = new Coluna();
        
        MetaData mt = new MetaData();
                        
        List<String> tabelas = mt.recuperaTabela();
        
        for (String tabela : tabelas) {
            Tabela tab = new Tabela();
            tab.setTabela(tabela);
            
            List<Coluna> colunas = mt.recuperaColunas(tab.getTabela());
            tab.setColunas(colunas);
            tabelaDAO.salvarTabela(tab);
        }
        
        
    }
    
}
