package br.com.upper.upintegra.persistencia;

import br.com.upper.upintegra.entidades.Coluna;
import br.com.upper.upintegra.entidades.Tabela;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author FILIPE
 */
public class TabelaDAO {

    private Conexao conexao;
    private Connection connection;

    public TabelaDAO() {
        conexao = Conexao.getInstance();
    }

    public void salvarTabela(Tabela tabela) throws SQLException {
        String sql = "insert into tabela (tabela, integra) values (?, ?)";

        connection = conexao.createConnection();
        PreparedStatement pst = connection.prepareStatement(sql);

        pst.setString(1, tabela.getTabela());
        pst.setString(2, tabela.getIntegra());
        pst.execute();

        pst.close();
        connection.close();

        salvarColunas(tabela.getTabela(), tabela.getColunas());
        atualizaPK(tabela);
        atualizaNN(tabela);
        atualizaUN(tabela);
    }

    public void salvarColunas(String tabela, List<Coluna> colunas) throws SQLException {
        String sql = "insert into coluna (tabela, coluna, tipo, tamanho, precisao, escala, "
                + "pk, nn, un) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";

        connection = conexao.createConnection();
        PreparedStatement pst = connection.prepareStatement(sql);

        for (Coluna coluna : colunas) {
            pst.setString(1, tabela);
            pst.setString(2, coluna.getColuna());
            pst.setString(3, coluna.getTipo());
            pst.setInt(4, coluna.getTamanho());
            pst.setInt(5, coluna.getPrecisao());
            pst.setInt(6, coluna.getEscala());
            pst.setBoolean(7, coluna.isPk());            
            pst.setBoolean(8, coluna.isNn());
            pst.setBoolean(9, coluna.isUn());
            pst.execute();
        }
        pst.close();

        connection.close();
    }

    public void atualizaPK(Tabela tabela) throws SQLException {
        MetaData mt = new MetaData();
        List<String> colunas = mt.listaPK(tabela);

        String sql = "update coluna set pk = true "
                + "where (coluna = ?) and (tabela = " + "'" + tabela.getTabela() + "')";

        connection = conexao.createConnection();
        PreparedStatement pst = connection.prepareStatement(sql);

        for (String coluna : colunas) {            
            pst.setString(1, coluna);            
            pst.executeUpdate();
        }

        pst.close();
        connection.close();
    }
    
    public void atualizaUN(Tabela tabela) throws SQLException {
        MetaData mt = new MetaData();
        List<String> colunas = mt.listaUN(tabela);

        String sql = "update coluna set un = true "
                + "where (coluna = ?) and (tabela = " + "'" + tabela.getTabela() + "')";

        connection = conexao.createConnection();
        PreparedStatement pst = connection.prepareStatement(sql);

        for (String coluna : colunas) {            
            pst.setString(1, coluna);            
            pst.executeUpdate();
        }

        pst.close();
        connection.close();
    }
    
    public void atualizaNN(Tabela tabela) throws SQLException {
        MetaData mt = new MetaData();
        List<String> colunas = mt.listaNN(tabela);

        String sql = "update coluna set nn = true "
                + "where (coluna = ?) and (tabela = " + "'" + tabela.getTabela() + "')";

        connection = conexao.createConnection();
        PreparedStatement pst = connection.prepareStatement(sql);

        for (String coluna : colunas) {            
            pst.setString(1, coluna);            
            pst.executeUpdate();
        }

        pst.close();
        connection.close();
    }

}
