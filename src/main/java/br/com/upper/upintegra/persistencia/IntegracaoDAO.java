package br.com.upper.upintegra.persistencia;

import br.com.upper.upintegra.entidades.Integridade;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author FILIPE
 */
public class IntegracaoDAO {
    
    private ConexaoTerceiro conexaoTerceiro;
    private Connection connection;

    public IntegracaoDAO() {
        conexaoTerceiro = ConexaoTerceiro.getInstance();
    }        
    
    public List<Integridade> listaIntegridades() throws SQLException{
        List<Integridade> integridades = new ArrayList<>();
        
        String sql = "select * from txd01 where status <> '0'";
        connection = conexaoTerceiro.createConnection();
        PreparedStatement pst = connection.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        
        while (rs.next()){
            Integridade integridade = new Integridade();
            integridade.setTabela(rs.getString("tabela"));
            integridade.setChave(rs.getString("chave"));
            integridade.setStatus(rs.getInt("status"));
            integridade.setData(rs.getTimestamp("data"));
            integridades.add(integridade);
        }
        connection.close();
        pst.close();
        rs.close();        
        
        return integridades;        
    }
    
    public void integrar(String status, String tabela, String query){
        String sql = null;
        if (status == "1"){
            sql = "inser into " + tabela + " " + query +" where = ";
        }else if (status == "2"){
            sql = "update " + tabela +" set ";            
        } else if (status == "3"){
            sql = "";
        }
    }
    
    
}
