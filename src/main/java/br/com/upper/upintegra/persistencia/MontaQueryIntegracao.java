package br.com.upper.upintegra.persistencia;

import br.com.upper.upintegra.entidades.Coluna;
import br.com.upper.upintegra.entidades.Integridade;
import br.com.upper.upintegra.entidades.Tabela;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author FILIPE
 */
public class MontaQueryIntegracao {

    private Conexao conexao;
    private Connection connection;

    public MontaQueryIntegracao() {
        conexao = Conexao.getInstance();
    }

    public String insertQuery(Tabela tabela) throws SQLException {
        List<Coluna> colunas = new ArrayList<>();
        String sql = "select * from coluna where tabela = ?";

        connection = conexao.createConnection();
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setString(1, tabela.getTabela());
        ResultSet rs = pst.executeQuery();

        while (rs.next()) {
            Coluna coluna = new Coluna();
            coluna.setColuna(rs.getString("coluna"));
            coluna.setTipo(rs.getString("tipo"));
            coluna.setTamanho(rs.getInt("tamanho"));
            coluna.setPrecisao(rs.getInt("precisao"));
            coluna.setEscala(rs.getInt("escala"));
            coluna.setPk(rs.getBoolean("pk"));
            coluna.setNn(rs.getBoolean("nn"));
            coluna.setUn(rs.getBoolean("un"));
            colunas.add(coluna);
        }
        rs.close();
        connection.close();
        pst.close();

        String insert = "";
        for (int i = 0; i < colunas.size(); i++) {
            if (i < colunas.size() - 1) {
                Coluna coluna = colunas.get(i);
                insert = insert + coluna.getColuna()+ ", ";
            } else {
                Coluna coluna = colunas.get(i);
                insert = insert + coluna.getColuna();
            }
        }
        return insert;

    }
    
    public String recuperaPK(Tabela tabela) throws SQLException {
        List<Coluna> colunas = new ArrayList<>();
        String sql = "select * from coluna where tabela = ? and pk = true";

        connection = conexao.createConnection();
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setString(1, tabela.getTabela());
        ResultSet rs = pst.executeQuery();

        while (rs.next()) {
            Coluna coluna = new Coluna();
            coluna.setColuna(rs.getString("coluna"));
            coluna.setTipo(rs.getString("tipo"));
            coluna.setTamanho(rs.getInt("tamanho"));
            coluna.setPrecisao(rs.getInt("precisao"));
            coluna.setEscala(rs.getInt("escala"));
            coluna.setPk(rs.getBoolean("pk"));
            coluna.setNn(rs.getBoolean("nn"));
            coluna.setUn(rs.getBoolean("un"));
            colunas.add(coluna);
        }
        rs.close();
        connection.close();
        pst.close();

        String pk = "(";
        for (int i = 0; i < colunas.size(); i++) {
            if (i < colunas.size() - 1) {
                Coluna coluna = colunas.get(i);
                pk = pk + coluna.getColuna()+ ", ";
            } else {
                Coluna coluna = colunas.get(i);
                pk = pk + coluna.getColuna()+ ")";
            }
        }
        return pk;
    }    

}
