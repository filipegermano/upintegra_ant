package br.com.upper.upintegra.persistencia;

import br.com.upper.upintegra.entidades.Coluna;
import br.com.upper.upintegra.entidades.Integridade;
import br.com.upper.upintegra.entidades.Tabela;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author FILIPE
 */
public class AtualizaMetaData {

    private Conexao conexao;
    private Connection connection;

    public AtualizaMetaData() {
        conexao = Conexao.getInstance();
    }

    public String queryTabelas(Tabela tabela) throws SQLException {
        List<Coluna> colunas = new ArrayList<>();
        String sql = "select * from coluna where tabela = ?";

        connection = conexao.createConnection();
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setString(1, tabela.getTabela());
        ResultSet rs = pst.executeQuery();

        while (rs.next()) {
            Coluna coluna = new Coluna();
            coluna.setColuna(rs.getString("coluna"));
            coluna.setTipo(rs.getString("tipo"));
            coluna.setTamanho(rs.getInt("tamanho"));
            coluna.setPrecisao(rs.getInt("precisao"));
            coluna.setEscala(rs.getInt("escala"));
            coluna.setPk(rs.getBoolean("pk"));
            coluna.setNn(rs.getBoolean("nn"));
            coluna.setUn(rs.getBoolean("un"));
            colunas.add(coluna);
        }
        rs.close();
        connection.close();
        pst.close();

        String pk = "(";
        String query = "CREATE TABLE " + tabela.getTabela() + "(";
        for (int i = 0; i < colunas.size(); i++) {
            if (i < colunas.size() - 1) {
                Coluna coluna = colunas.get(i);
                if ((coluna.getTipo().equals("VARCHAR")) || (coluna.getTipo().equals("CHAR"))) {
                    if (coluna.isNn() && coluna.isUn()) {
                        query = query
                                + coluna.getColuna() + " "
                                + coluna.getTipo()
                                + "(" + coluna.getTamanho() + ") NOT NULL UNIQUE, ";
                    } else if (coluna.isNn()) {
                        query = query
                                + coluna.getColuna() + " "
                                + coluna.getTipo()
                                + "(" + coluna.getTamanho() + ") NOT NULL, ";
                    } else if (coluna.isUn()) {
                        query = query
                                + coluna.getColuna() + " "
                                + coluna.getTipo()
                                + "(" + coluna.getTamanho() + ") UNIQUE, ";
                    } else {
                        query = query
                                + coluna.getColuna() + " "
                                + coluna.getTipo()
                                + "(" + coluna.getTamanho() + "), ";
                    }
                } else if ((coluna.getTipo().equals("NUMERIC")) || (coluna.getTipo().equals("DECIMAL"))) {
                    if (coluna.isNn() && coluna.isUn()) {
                        query = query
                                + coluna.getColuna() + " "
                                + coluna.getTipo()
                                + "(" + coluna.getPrecisao() + ","
                                + coluna.getEscala() + ") NOT NULL UNIQUE, ";
                    } else if (coluna.isNn()) {
                        query = query
                                + coluna.getColuna() + " "
                                + coluna.getTipo()
                                + "(" + coluna.getPrecisao() + ","
                                + coluna.getEscala() + ") NOT NULL, ";
                    } else if (coluna.isUn()) {
                        query = query
                                + coluna.getColuna() + " "
                                + coluna.getTipo()
                                + "(" + coluna.getPrecisao() + ","
                                + coluna.getEscala() + ") UNIQUE, ";
                    } else {
                        query = query
                                + coluna.getColuna() + " "
                                + coluna.getTipo()
                                + "(" + coluna.getPrecisao() + ","
                                + coluna.getEscala() + "), ";
                    }
                } else {
                    if (coluna.isNn() && coluna.isUn()) {
                        query = query + coluna.getColuna() + " " + coluna.getTipo() + ", NOT NULL UNIQUE, ";
                    } else if (coluna.isNn()) {
                        query = query + coluna.getColuna() + " " + coluna.getTipo() + ", NOT NULL, ";
                    } else if (coluna.isUn()) {
                        query = query + coluna.getColuna() + " " + coluna.getTipo() + ", UNIQUE, ";
                    } else {
                        query = query + coluna.getColuna() + " " + coluna.getTipo() + ", ";
                    }
                }
                if (coluna.isPk()) {
                    pk = pk + coluna.getColuna() + ", ";
                }
            } else {
                Coluna coluna = colunas.get(i);
                if ((coluna.getTipo().equals("VARCHAR")) || (coluna.getTipo().equals("CHAR"))) {
                    if (coluna.isNn() && coluna.isUn()) {
                        query = query
                                + coluna.getColuna() + " "
                                + coluna.getTipo()
                                + "(" + coluna.getTamanho() + ") NOT NULL UNIQUE) ";
                    } else if (coluna.isNn()) {
                        query = query
                                + coluna.getColuna() + " "
                                + coluna.getTipo()
                                + "(" + coluna.getTamanho() + ") NOT NULL) ";
                    } else if (coluna.isUn()) {
                        query = query
                                + coluna.getColuna() + " "
                                + coluna.getTipo()
                                + "(" + coluna.getTamanho() + ") UNIQUE) ";
                    } else {
                        query = query
                                + coluna.getColuna() + " "
                                + coluna.getTipo()
                                + "(" + coluna.getTamanho() + ")) ";
                    }
                } else if ((coluna.getTipo().equals("NUMERIC")) || (coluna.getTipo().equals("DECIMAL"))) {
                    if (coluna.isNn() && coluna.isUn()) {
                        query = query
                                + coluna.getColuna() + " "
                                + coluna.getTipo()
                                + "(" + coluna.getPrecisao() + ","
                                + coluna.getEscala() + ") NOT NULL UNIQUE) ";
                    } else if (coluna.isNn()) {
                        query = query
                                + coluna.getColuna() + " "
                                + coluna.getTipo()
                                + "(" + coluna.getPrecisao() + ","
                                + coluna.getEscala() + ") NOT NULL) ";
                    } else if (coluna.isUn()) {
                        query = query
                                + coluna.getColuna() + " "
                                + coluna.getTipo()
                                + "(" + coluna.getPrecisao() + ","
                                + coluna.getEscala() + ") UNIQUE) ";
                    } else {
                        query = query
                                + coluna.getColuna() + " "
                                + coluna.getTipo()
                                + "(" + coluna.getPrecisao() + ","
                                + coluna.getEscala() + ")) ";
                    }
                } else {
                    if (coluna.isNn() && coluna.isUn()) {
                        query = query + coluna.getColuna() + " " + coluna.getTipo() + ", NOT NULL UNIQUE) ";
                    } else if (coluna.isNn()) {
                        query = query + coluna.getColuna() + " " + coluna.getTipo() + ", NOT NULL) ";
                    } else if (coluna.isUn()) {
                        query = query + coluna.getColuna() + " " + coluna.getTipo() + ", UNIQUE) ";
                    } else {
                        query = query + coluna.getColuna() + " " + coluna.getTipo() + ") ";
                    }
                }
                if (coluna.isPk()) {
                    pk = pk + coluna.getColuna();
                }
            }
        }

        pk = pk + ");";
        pk = pk.replace(", );", ");");

        query = query + ";" + "ALTER TABLE " + tabela.getTabela() + " ADD PRIMARY KEY " + pk;
        return query;
    }

    //rotina retorna query completa em um unica linha --- descontinuada
//    public String queryColunas(Tabela tabela) throws SQLException {
//        List<Coluna> colunas = new ArrayList<>();
//        String sql = "select * from coluna where tabela = ?";
//
//        connection = conexao.createConnection();
//        PreparedStatement pst = connection.prepareStatement(sql);
//        pst.setString(1, tabela.getTabela());
//        ResultSet rs = pst.executeQuery();
//
//        while (rs.next()) {
//            Coluna coluna = new Coluna();
//            coluna.setColuna(rs.getString("coluna"));
//            coluna.setTipo(rs.getString("tipo"));
//            coluna.setTamanho(rs.getInt("tamanho"));
//            coluna.setPrecisao(rs.getInt("precisao"));
//            coluna.setEscala(rs.getInt("escala"));
//            coluna.setPk(rs.getBoolean("pk"));
//            coluna.setNn(rs.getBoolean("nn"));
//            coluna.setUn(rs.getBoolean("un"));
//            colunas.add(coluna);
//        }
//        rs.close();
//        connection.close();
//        pst.close();
//
//        String pk = "(";
//        String query = "";// = "CREATE TABLE " + tabela.getTabela() + "(";
//        for (int i = 0; i < colunas.size(); i++) {
//            Coluna coluna = colunas.get(i);
//            if ((coluna.getTipo().equals("VARCHAR")) || (coluna.getTipo().equals("CHAR"))) {
//                if (coluna.isNn() && coluna.isUn()) {
//                    query = query + "ALTER TABLE " + tabela.getTabela() + " ADD "
//                            + coluna.getColuna() + " "
//                            + coluna.getTipo()
//                            + "(" + coluna.getTamanho() + ") NOT NULL UNIQUE; ";
//                } else if (coluna.isNn()) {
//                    query = query + "ALTER TABLE " + tabela.getTabela() + " ADD "
//                            + coluna.getColuna() + " "
//                            + coluna.getTipo()
//                            + "(" + coluna.getTamanho() + ") NOT NULL; ";
//                } else if (coluna.isUn()) {
//                    query = query + "ALTER TABLE " + tabela.getTabela() + " ADD "
//                            + coluna.getColuna() + " "
//                            + coluna.getTipo()
//                            + "(" + coluna.getTamanho() + ") UNIQUE; ";
//                } else {
//                    query = query + "ALTER TABLE " + tabela.getTabela() + " ADD "
//                            + coluna.getColuna() + " "
//                            + coluna.getTipo()
//                            + "(" + coluna.getTamanho() + "); ";
//                }
//            } else if ((coluna.getTipo().equals("NUMERIC")) || (coluna.getTipo().equals("DECIMAL"))) {
//                if (coluna.isNn() && coluna.isUn()) {
//                    query = query + "ALTER TABLE " + tabela.getTabela() + " ADD "
//                            + coluna.getColuna() + " "
//                            + coluna.getTipo()
//                            + "(" + coluna.getPrecisao() + ","
//                            + coluna.getEscala() + ") NOT NULL UNIQUE; ";
//                } else if (coluna.isNn()) {
//                    query = query + "ALTER TABLE " + tabela.getTabela() + " ADD "
//                            + coluna.getColuna() + " "
//                            + coluna.getTipo()
//                            + "(" + coluna.getPrecisao() + ","
//                            + coluna.getEscala() + ") NOT NULL; ";
//                } else if (coluna.isUn()) {
//                    query = query + "ALTER TABLE " + tabela.getTabela() + " ADD "
//                            + coluna.getColuna() + " "
//                            + coluna.getTipo()
//                            + "(" + coluna.getPrecisao() + ","
//                            + coluna.getEscala() + ") UNIQUE; ";
//                } else {
//                    query = query + "ALTER TABLE " + tabela.getTabela() + " ADD "
//                            + coluna.getColuna() + " "
//                            + coluna.getTipo()
//                            + "(" + coluna.getPrecisao() + ","
//                            + coluna.getEscala() + "); ";
//                }
//            } else {
//                if (coluna.isNn() && coluna.isUn()) {
//                    query = query + "ALTER TABLE " + tabela.getTabela() + " ADD "
//                            + coluna.getColuna() + " " 
//                            + coluna.getTipo() + ", NOT NULL UNIQUE; ";
//                } else if (coluna.isNn()) {
//                    query = query + "ALTER TABLE " + tabela.getTabela() + " ADD " 
//                            + coluna.getColuna() + " " 
//                            + coluna.getTipo() + ", NOT NULL; ";
//                } else if (coluna.isUn()) {
//                    query = query + "ALTER TABLE " + tabela.getTabela() + " ADD " 
//                            + coluna.getColuna() + " " 
//                            + coluna.getTipo() + ", UNIQUE; ";
//                } else {
//                    query = query + "ALTER TABLE " + tabela.getTabela() + " ADD " 
//                            + coluna.getColuna() + " " 
//                            + coluna.getTipo() + "; ";
//                }
//            }
//        }
//        return query;
//    }
    public List<String> queryColuna(Tabela tabela) throws SQLException {
        List<Coluna> colunas = new ArrayList<>();
        String sql = "select * from coluna where tabela = ?";

        connection = conexao.createConnection();
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setString(1, tabela.getTabela());
        ResultSet rs = pst.executeQuery();

        while (rs.next()) {
            Coluna coluna = new Coluna();
            coluna.setColuna(rs.getString("coluna"));
            coluna.setTipo(rs.getString("tipo"));
            coluna.setTamanho(rs.getInt("tamanho"));
            coluna.setPrecisao(rs.getInt("precisao"));
            coluna.setEscala(rs.getInt("escala"));
            coluna.setPk(rs.getBoolean("pk"));
            coluna.setNn(rs.getBoolean("nn"));
            coluna.setUn(rs.getBoolean("un"));
            colunas.add(coluna);
        }
        rs.close();
        connection.close();
        pst.close();

        List<String> addColunas = new ArrayList<>();
        String query = "";// = "CREATE TABLE " + tabela.getTabela() + "(";
        for (int i = 0; i < colunas.size(); i++) {
            Coluna coluna = colunas.get(i);
            if ((coluna.getTipo().equals("VARCHAR")) || (coluna.getTipo().equals("CHAR"))) {
                if (coluna.isNn() && coluna.isUn()) {
                    query = "ALTER TABLE " + tabela.getTabela() + " ADD "
                            + coluna.getColuna() + " "
                            + coluna.getTipo()
                            + "(" + coluna.getTamanho() + ") NOT NULL UNIQUE; ";
                    addColunas.add(query);
                } else if (coluna.isNn()) {
                    query = "ALTER TABLE " + tabela.getTabela() + " ADD "
                            + coluna.getColuna() + " "
                            + coluna.getTipo()
                            + "(" + coluna.getTamanho() + ") NOT NULL; ";
                    addColunas.add(query);
                } else if (coluna.isUn()) {
                    query = "ALTER TABLE " + tabela.getTabela() + " ADD "
                            + coluna.getColuna() + " "
                            + coluna.getTipo()
                            + "(" + coluna.getTamanho() + ") UNIQUE; ";
                    addColunas.add(query);
                } else {
                    query = "ALTER TABLE " + tabela.getTabela() + " ADD "
                            + coluna.getColuna() + " "
                            + coluna.getTipo()
                            + "(" + coluna.getTamanho() + "); ";
                    addColunas.add(query);
                }
            } else if ((coluna.getTipo().equals("NUMERIC")) || (coluna.getTipo().equals("DECIMAL"))) {
                if (coluna.isNn() && coluna.isUn()) {
                    query = "ALTER TABLE " + tabela.getTabela() + " ADD "
                            + coluna.getColuna() + " "
                            + coluna.getTipo()
                            + "(" + coluna.getPrecisao() + ","
                            + coluna.getEscala() + ") NOT NULL UNIQUE; ";
                    addColunas.add(query);
                } else if (coluna.isNn()) {
                    query = "ALTER TABLE " + tabela.getTabela() + " ADD "
                            + coluna.getColuna() + " "
                            + coluna.getTipo()
                            + "(" + coluna.getPrecisao() + ","
                            + coluna.getEscala() + ") NOT NULL; ";
                    addColunas.add(query);
                } else if (coluna.isUn()) {
                    query = "ALTER TABLE " + tabela.getTabela() + " ADD "
                            + coluna.getColuna() + " "
                            + coluna.getTipo()
                            + "(" + coluna.getPrecisao() + ","
                            + coluna.getEscala() + ") UNIQUE; ";
                    addColunas.add(query);
                } else {
                    query = "ALTER TABLE " + tabela.getTabela() + " ADD "
                            + coluna.getColuna() + " "
                            + coluna.getTipo()
                            + "(" + coluna.getPrecisao() + ","
                            + coluna.getEscala() + "); ";
                    addColunas.add(query);
                }
            } else {
                if (coluna.isNn() && coluna.isUn()) {
                    query = "ALTER TABLE " + tabela.getTabela() + " ADD "
                            + coluna.getColuna() + " "
                            + coluna.getTipo() + ", NOT NULL UNIQUE; ";
                    addColunas.add(query);
                } else if (coluna.isNn()) {
                    query = "ALTER TABLE " + tabela.getTabela() + " ADD "
                            + coluna.getColuna() + " "
                            + coluna.getTipo() + ", NOT NULL; ";
                    addColunas.add(query);
                } else if (coluna.isUn()) {
                    query = "ALTER TABLE " + tabela.getTabela() + " ADD "
                            + coluna.getColuna() + " "
                            + coluna.getTipo() + ", UNIQUE; ";
                    addColunas.add(query);
                } else {
                    query = "ALTER TABLE " + tabela.getTabela() + " ADD "
                            + coluna.getColuna() + " "
                            + coluna.getTipo() + "; ";
                    addColunas.add(query);
                }
            }
        }
        return addColunas;
    }

    public String executeQueryTabela(String query) {
        try {
            String sql = query;

            connection = conexao.createConnection();
            PreparedStatement pst = connection.prepareStatement(sql);
            pst.execute();
            pst.close();
            connection.close();
            return "Tabela criada com sucesso!!!";
        } catch (SQLException ex) {
            //Logger.getLogger(AtualizaMetaData.class.getName()).log(Level.SEVERE, null, ex);
            return "Tabela ja existente!!!";
        }
    }

    public String executeQueryColuna(String query) {
        connection = conexao.createConnection();
        PreparedStatement pst = null;
        try {
            //for (String query : querys) {
            //String sql = query;
            connection = conexao.createConnection();
            pst = connection.prepareStatement(query);
            pst.execute();
            pst.close();
            connection.close();
            //}
            return "Colunas atualizadas com sucesso!!!";
        } catch (SQLException ex) {
            //Logger.getLogger(AtualizaMetaData.class.getName()).log(Level.SEVERE, null, ex);            
            return "Coluna ja existente!!!";
        } finally {
            try {
                pst.close();
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(AtualizaMetaData.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    public String criaTrigger(Tabela tabela) throws SQLException {
        List<Coluna> colunas = new ArrayList<>();
        String sql = "select * from coluna where tabela = ?";

        connection = conexao.createConnection();
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setString(1, tabela.getTabela());
        ResultSet rs = pst.executeQuery();

        while (rs.next()) {
            Coluna coluna = new Coluna();
            coluna.setColuna(rs.getString("coluna"));
            coluna.setTipo(rs.getString("tipo"));
            coluna.setTamanho(rs.getInt("tamanho"));
            coluna.setPrecisao(rs.getInt("precisao"));
            coluna.setEscala(rs.getInt("escala"));
            coluna.setPk(rs.getBoolean("pk"));
            coluna.setNn(rs.getBoolean("nn"));
            coluna.setUn(rs.getBoolean("un"));
            colunas.add(coluna);
        }
        rs.close();
        connection.close();
        pst.close();

        String pk = "(";
        String queryIU = "CREATE OR ALTER TRIGGER " + tabela.getTabela() + "_IIUD0 FOR " + tabela.getTabela()
                + " ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 0 "
                + "AS BEGIN IF (INSERTING OR UPDATING) THEN "
                + "INSERT INTO AXI (XI_TABELA, XI_CHAVE, XI_STATUS, XI_DATA) "
                + "VALUES ('" + tabela.getTabela() + "', 'UPDATE OR INSERT INTO " + tabela.getTabela() + " (";
        String queryD = "ELSE IF (DELETING) THEN "
                + "INSERT INTO AXI (XI_TABELA, XI_CHAVE, XI_STATUS, XI_DATA) "
                + "VALUES ('" + tabela.getTabela() + "', 'DELETE FROM " + tabela.getTabela() + " WHERE ";
        //pega campos
        for (int i = 0; i < colunas.size(); i++) {
            if (i < colunas.size() - 1) {
                Coluna coluna = colunas.get(i);
                queryIU = queryIU + coluna.getColuna() + ", ";

                if (coluna.isPk()) {
                    pk = pk + coluna.getColuna() + ", ";
                    if (coluna.getTipo().equals("VARCHAR") || coluna.getTipo().equals("CHAR")){
                        queryD = queryD + coluna.getColuna() + " = '||''''||OLD." + coluna.getColuna() + "||''''||' AND '||'";
                    }else{
                        queryD = queryD + coluna.getColuna() + " = '||OLD." + coluna.getColuna() + "||' AND '||'";
                    }                    
                }
            } else {
                Coluna coluna = colunas.get(i);
                queryIU = queryIU + coluna.getColuna() + ") ";
                if (coluna.isPk()) {
                    pk = pk + coluna.getColuna();
                    if (coluna.getTipo().equals("VARCHAR") || coluna.getTipo().equals("CHAR")){
                        queryD = queryD + coluna.getColuna() + " = '||''''||OLD." + coluna.getColuna() + "||''''";
                    }else{
                        queryD = queryD + coluna.getColuna() + " = '||OLD." + coluna.getColuna();
                    }
                    
                }
            }
        }

        queryIU = queryIU + "VALUES ('||";
        for (int i = 0; i < colunas.size(); i++) {
            if (i < colunas.size() - 1) {
                Coluna coluna = colunas.get(i);
                if (coluna.getTipo().equals("VARCHAR") || coluna.getTipo().equals("CHAR")
                        || coluna.getTipo().equals("DATE") || coluna.getTipo().equals("TIME")) {
                    queryIU = queryIU + "''''||COALESCE(NEW." + coluna.getColuna() + ",'NULL')||''''||', '||";
                } else {
                    queryIU = queryIU + "COALESCE(NEW." + coluna.getColuna() + ",'NULL')||', '||";
                }
            } else {
                Coluna coluna = colunas.get(i);
                if (coluna.getTipo().equals("VARCHAR") || coluna.getTipo().equals("CHAR")
                        || coluna.getTipo().equals("DATE") || coluna.getTipo().equals("TIME")) {
                    queryIU = queryIU + "''''||COALESCE(NEW." + coluna.getColuna() + ",'NULL')||''')";
                } else {
                    queryIU = queryIU + "COALESCE(NEW." + coluna.getColuna() + ",'NULL')||')";
                }
            }
        }

        //queryIU = queryIU + ", "
        pk = pk + ");";
        pk = pk.replace(", );", ")");

        pk = pk + "', 1, CURRENT_TIMESTAMP);";        
        queryD = queryD + ", 2, CURRENT_TIMESTAMP);";
        queryD = queryD.replace("||' AND '||',", ",");
        queryIU = queryIU + "MATCHING " + pk + " " + queryD + " END";        
        return queryIU;
//        return queryD;
    }
        
    
}
