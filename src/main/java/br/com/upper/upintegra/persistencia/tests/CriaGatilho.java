package br.com.upper.upintegra.persistencia.tests;

import br.com.upper.upintegra.entidades.Tabela;
import br.com.upper.upintegra.persistencia.AtualizaMetaData;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author FILIPE
 */
public class CriaGatilho {
    
    public static void main(String[] args) {
        AtualizaMetaData a = new AtualizaMetaData();
        Tabela t = new Tabela();
        t.setTabela("TI101");
        
        try {
//            System.out.println(a.criaTrigger(t));
            
            System.out.println(a.executeQueryTabela(a.criaTrigger(t)));
        } catch (SQLException ex) {
            Logger.getLogger(CriaGatilho.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
