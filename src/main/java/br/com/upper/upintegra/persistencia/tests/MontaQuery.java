package br.com.upper.upintegra.persistencia.tests;

import br.com.upper.upintegra.entidades.Integridade;
import br.com.upper.upintegra.entidades.Tabela;
import br.com.upper.upintegra.persistencia.IntegridadeDAO;
import br.com.upper.upintegra.persistencia.MontaQueryIntegracao;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author FILIPE
 */
public class MontaQuery {
    
    public static void main(String[] args) throws SQLException {
        Tabela tabela = new Tabela();
                
        tabela.setTabela("TA101");
        
        MontaQueryIntegracao integracao = new MontaQueryIntegracao();
        
        System.out.println(integracao.insertQuery(tabela));
        System.out.println(integracao.recuperaPK(tabela));
        
        IntegridadeDAO integridadeDAO = new IntegridadeDAO();                
                
        List<Integridade> integridades = integridadeDAO.listaIntegridades();
        
        for (Integridade integridade : integridades) {
            System.out.println(integridade.getTabela() +" - "+ integridade.getChave() + " - "
                    + integridade.getData());
            //integridadeDAO.updateStatus(integridade);
            System.out.println(integridadeDAO.recuperaIntegridade(integridade));
        }                        
    }
    
}
