package br.com.upper.upintegra.persistencia;

import br.com.upper.upintegra.entidades.Coluna;
import br.com.upper.upintegra.entidades.Integridade;
import br.com.upper.upintegra.entidades.Tabela;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author FILIPE
 */
public class MetaData {

    private ConexaoTerceiro conexaoTerceiro;
    private Connection connection;

    public MetaData() {
        conexaoTerceiro = ConexaoTerceiro.getInstance();
    }

    public List<String> recuperaTabela() {
        try {
            List<String> tabelas = new ArrayList<>();

            String sql = "select rdb$relation_name from rdb$relations where rdb$system_flag = 0";
            connection = conexaoTerceiro.createConnection();
            PreparedStatement pst = connection.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                if (rs.getString(1).trim().length() == 5) {
                    tabelas.add(rs.getString(1));
                }
            }
            rs.close();
            connection.close();
            pst.close();
            return tabelas;
        } catch (SQLException ex) {
            Logger.getLogger(MetaData.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public List<Coluna> recuperaColunas(String tabela) throws SQLException {

        connection = conexaoTerceiro.createConnection();
        DatabaseMetaData mt = connection.getMetaData();
        Statement stmt = connection.createStatement();
        // Tabela a ser analisada  
        ResultSet rset = stmt.executeQuery("SELECT * from " + tabela);

        ResultSetMetaData rsmd = rset.getMetaData();

        // retorna o numero total de colunas  
        int numColumns = rsmd.getColumnCount();
        System.out.println("Total de Colunas = " + numColumns);

        // loop para recuperar os metadados de cada coluna  
        List<Coluna> colunas = new ArrayList<>();

        for (int i = 0; i < numColumns; i++) {
            Coluna coluna = new Coluna();
//            coluna.set rsmd.getTableName(i + 1));
            coluna.setColuna(rsmd.getColumnName(i + 1));
            coluna.setTipo(rsmd.getColumnTypeName(i + 1));
            coluna.setTamanho(rsmd.getColumnDisplaySize(i + 1));
            coluna.setPrecisao(rsmd.getPrecision(i + 1));
            coluna.setEscala(rsmd.getScale(i + 1));
            colunas.add(coluna);
        }
        return colunas;
    }

    public List<String> listaPK(Tabela tabela) {
        try {
            List<String> colunas = new ArrayList<>();

            String sql = "select idx.RDB$FIELD_NAME"
                    + " from RDB$RELATION_CONSTRAINTS tc"
                    + " join RDB$INDEX_SEGMENTS idx on (idx.RDB$INDEX_NAME = tc.RDB$INDEX_NAME)"
                    + " where tc.RDB$CONSTRAINT_TYPE = 'PRIMARY KEY'"
                    + " and tc.RDB$RELATION_NAME = " + "'" + tabela.getTabela() + "'"
                    + " order by idx.RDB$FIELD_POSITION";
            connection = conexaoTerceiro.createConnection();
            PreparedStatement pst = connection.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                colunas.add(rs.getString(1).trim());
            }
            rs.close();
            connection.close();
            pst.close();
            return colunas;
        } catch (SQLException ex) {
            Logger.getLogger(MetaData.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public List<String> listaUN(Tabela tabela) {
        try {
            List<String> colunas = new ArrayList<>();

            String sql = "select idx.RDB$FIELD_NAME"
                    + " from RDB$RELATION_CONSTRAINTS tc"
                    + " join RDB$INDEX_SEGMENTS idx on (idx.RDB$INDEX_NAME = tc.RDB$INDEX_NAME)"
                    + " where tc.RDB$CONSTRAINT_TYPE = 'UNIQUE'"
                    + " and tc.RDB$RELATION_NAME = " + "'" + tabela.getTabela() + "'"
                    + " order by idx.RDB$FIELD_POSITION";
            connection = conexaoTerceiro.createConnection();
            PreparedStatement pst = connection.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                colunas.add(rs.getString(1).trim());
            }
            rs.close();
            connection.close();
            pst.close();
            return colunas;
        } catch (SQLException ex) {
            Logger.getLogger(MetaData.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public List<String> listaNN(Tabela tabela) {
        try {
            List<String> colunas = new ArrayList<>();

            String sql = "select rdb$field_name"
                    + " from rdb$relation_fields"
                    + " where rdb$relation_name in("
                    + " select rdb$relation_name"
                    + " from rdb$relations"
                    + " where rdb$system_flag = 0"
                    + " and rdb$relation_name = '" + tabela.getTabela() + "')"
                    + " and rdb$null_flag = 1";
            connection = conexaoTerceiro.createConnection();
            PreparedStatement pst = connection.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                colunas.add(rs.getString(1).trim());
            }
            rs.close();
            connection.close();
            pst.close();
            return colunas;
        } catch (SQLException ex) {
            Logger.getLogger(MetaData.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public String executeQueryTrigger(String query) {

        try {
            connection = conexaoTerceiro.createConnection();
            PreparedStatement pst = connection.prepareStatement(query);
            pst.execute();
            pst.close();

            pst.close();
            connection.close();
            return "Colunas atualizadas com sucesso!!!";
        } catch (SQLException ex) {
            //Logger.getLogger(AtualizaMetaData.class.getName()).log(Level.SEVERE, null, ex);            
            return "Coluna ja existente!!!";
        }
    }

    public List<Integridade> listaIntegridades() {
        try {
            List<Integridade> integridades = new ArrayList<>();

            String sql = "select * from axi where xi_status = 1";
            connection = conexaoTerceiro.createConnection();
            PreparedStatement pst = connection.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                Integridade integridade = new Integridade();
                integridade.setId(rs.getLong("xi_id"));
                integridade.setTabela(rs.getString("xi_tabela"));
                integridade.setChave(rs.getString("xi_chave"));
                integridade.setData(rs.getTimestamp("xi_data"));
                integridade.setStatus(rs.getInt("xi_status"));
                integridades.add(integridade);
            }
            rs.close();
            connection.close();
            pst.close();
            return integridades;
        } catch (SQLException ex) {
            Logger.getLogger(MetaData.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public void trataQuery() throws SQLException {
        List<Integridade> integridades = listaIntegridades();
        List<Integridade> inteAlter = new ArrayList<>();

        for (Integridade integridade : integridades) {
            Integridade integridade1 = new Integridade();
            integridade1.setId(integridade.getId());
            integridade1.setTabela(integridade.getTabela());
            integridade1.setChave(integridade.getChave().replaceAll("'NULL'", "NULL").replaceAll("''", "NULL"));
            integridade1.setData(integridade.getData());
            integridade1.setStatus(2);
            inteAlter.add(integridade1);
        }

        for (Integridade integridade : inteAlter) {
            String sql = "update axi set xi_chave = ?, xi_status = ? where xi_id = ? ";
            connection = conexaoTerceiro.createConnection();
            PreparedStatement pst = connection.prepareStatement(sql);

            pst.setString(1, integridade.getChave());
            pst.setInt(2, integridade.getStatus());
            pst.setLong(3, integridade.getId());

            pst.executeUpdate();
            pst.close();
            connection.close();
        }
    }

}
