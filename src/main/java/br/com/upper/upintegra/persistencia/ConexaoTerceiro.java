package br.com.upper.upintegra.persistencia;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author FILIPE
 */
public class ConexaoTerceiro {

    private String driver, path, login, senha;
    private static ConexaoTerceiro instancia;

    private ConexaoTerceiro() {                                
        this.driver = "org.firebirdsql.jdbc.FBDriver";
        this.path = "jdbc:firebirdsql:localhost/3050:D:/InfoLight/Dados/BASE_CAJA.fdb";
        this.login = "SYSDBA";
        this.senha = "masterkey";
    }        

    public static Properties getProp() throws IOException {
        Properties props = new Properties();
        FileInputStream file = new FileInputStream("./properties/dadosTerceiros.properties");
        props.load(file);
        return props;
    }

    public static ConexaoTerceiro getInstance() {
        if (instancia == null) {
            return instancia = new ConexaoTerceiro();
        }
        return instancia;
    }

    public Connection createConnection() {
        try {
            Class.forName(this.driver);
            return DriverManager.getConnection(path, login, senha);
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ConexaoTerceiro.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
