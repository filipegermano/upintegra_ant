package br.com.upper.upintegra.entidades;

/**
 *
 * @author FILIPE
 */
public class Gatilho {

    private String nome;
    private String query;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

}
