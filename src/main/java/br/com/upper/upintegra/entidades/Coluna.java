package br.com.upper.upintegra.entidades;

/**
 *
 * @author FILIPE
 */
public class Coluna {

    private String coluna;
    private String tipo;
    private Integer tamanho;
    private Integer precisao;
    private Integer escala;
    private boolean pk;    
    private boolean nn;
    private boolean un;

    public String getColuna() {
        return coluna;
    }

    public void setColuna(String coluna) {
        this.coluna = coluna.trim();
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo.trim();
    }

    public Integer getTamanho() {
        return tamanho;
    }

    public void setTamanho(Integer tamanho) {
        this.tamanho = tamanho;
    }

    public Integer getPrecisao() {
        return precisao;
    }

    public void setPrecisao(Integer precisao) {
        this.precisao = precisao;
    }

    public Integer getEscala() {
        return escala;
    }

    public void setEscala(Integer escala) {
        this.escala = escala;
    }

    public boolean isPk() {
        return pk;
    }

    public void setPk(boolean pk) {
        this.pk = pk;
    }    

    public boolean isNn() {
        return nn;
    }

    public void setNn(boolean nn) {
        this.nn = nn;
    }

    public boolean isUn() {
        return un;
    }

    public void setUn(boolean un) {
        this.un = un;
    }

}
