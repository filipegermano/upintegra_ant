package br.com.upper.upintegra.entidades;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author FILIPE
 */
public class Integridade {

    private Long id;
    private String tabela;
    private String chave;
    private Integer status;
    private Date data;

    public String getTabela() {
        return tabela;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }    
    
    public void setTabela(String tabela) {
        this.tabela = tabela;
    }

    public String getChave() {
        return chave;
    }

    public void setChave(String chave) {
        this.chave = chave;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

}
