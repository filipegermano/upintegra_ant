package br.com.upper.upintegra.entidades;

import java.util.List;

/**
 *
 * @author FILIPE
 */
public class Tabela {

    private String tabela;
    private String integra;
    private List<Coluna> colunas;
    private List<Indice> indices;
    private List<Gatilho> gatilhos;

    public String getTabela() {
        return tabela;
    }

    public void setTabela(String tabela) {
        this.tabela = tabela.trim();
    }

    public String getIntegra() {
        return integra;
    }

    public void setIntegra(String integra) {
        this.integra = integra.trim();
    }

    public List<Coluna> getColunas() {
        return colunas;
    }

    public void setColunas(List<Coluna> colunas) {
        this.colunas = colunas;
    }

    public List<Indice> getIndices() {
        return indices;
    }

    public void setIndices(List<Indice> indices) {
        this.indices = indices;
    }

    public List<Gatilho> getGatilhos() {
        return gatilhos;
    }

    public void setGatilhos(List<Gatilho> gatilhos) {
        this.gatilhos = gatilhos;
    }

}
